#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <time.h>

using namespace std;

const int nMaxAge = 10;
const float fUnitEnergy = 1.F;
const unsigned nUniverseSize = 1000;
const unsigned nRanks = 2;

int BigRand()
{
	return rand()*RAND_MAX + rand();
}

struct Species
{
	int nAge;
	int nMaxAge;
	float fAliveEnergy;
	float fBasicEnergy;
	float fDayEnergy;
	float fEnergy;

	int nRank;
	//int nCover;

	bool bDead;
	
	Species(int nMaxAge_, float fAliveEnergy_, float fBasicEnergy_, int nRank_, float fDayEnergy_)
		:nAge(0), nMaxAge(nMaxAge_), fAliveEnergy(fAliveEnergy_),
		fBasicEnergy(fBasicEnergy_), fEnergy(fBasicEnergy_), fDayEnergy(fDayEnergy_),
		nRank(nRank_), bDead(false)
	{}
};

void PutNew(int nRank, int nUnv);

struct Universe
{
	vector<Species*> vOldPopulation;
	vector<Species*> vPopulation;
	vector< vector<Species*> > vWorld;

	int nNum;

	Universe(int nNum_):nNum(nNum_), vWorld(nUniverseSize, vector<Species*>()){}

	void Live()
	{
		size_t i;
		for(i = 0; i < nUniverseSize; ++i)
			vWorld[i].clear();

		for(i = 0; i < vPopulation.size(); ++i)
			vWorld[BigRand()%nUniverseSize].push_back(vPopulation[i]);

		for(i = 0; i < nUniverseSize; ++i)
		{
			vector<Species*>& v = vWorld[i];

			vector< vector<Species*> > vSpeciesByRank(nRanks, vector<Species*>());
			
			for(size_t j = 0, sz = v.size(); j < sz; ++j)
				vSpeciesByRank[v[j]->nRank].push_back(v[j]);

			for(size_t nRank = 0; nRank < nRanks; ++nRank)
			{
				if(nRank == 0)
				{
					for(size_t k = 0, sz = vSpeciesByRank[nRank].size(); k < sz; ++k)
						vSpeciesByRank[nRank][k]->fEnergy += fUnitEnergy/sz;
					continue;
				}

				//if(vSpeciesByRank[nRank].size() > 1 && rand()%12 > 7)
				//if(vSpeciesByRank[nRank].size() > 1 && rand()%2)
				//	continue;

				for(size_t k = 0, sz = vSpeciesByRank[nRank].size(); k < sz; ++k)
				{
					if(vSpeciesByRank[nRank - 1].empty())
						continue;
					Species* pPrey = vSpeciesByRank[nRank - 1][rand()%vSpeciesByRank[nRank - 1].size()];
					if(pPrey->bDead)
						continue;
					pPrey->bDead = true;
					vSpeciesByRank[nRank][k]->fEnergy += pPrey->fEnergy;
				}
			}
		}

		vOldPopulation = vPopulation;
		vPopulation.clear();
	}

	void Advance()
	{
		for(size_t i = 0; i < vOldPopulation.size(); ++i)
		{
			Species* pSp = vOldPopulation[i];
			
			if(pSp->bDead)
			{
				delete pSp;
				continue;
			}

			pSp->fEnergy -= pSp->fDayEnergy;
			if(pSp->fEnergy < pSp->fAliveEnergy)
			{
				delete pSp;
				continue;
			}

			while(pSp->fEnergy >= pSp->fBasicEnergy*2)
			{
				pSp->fEnergy -= pSp->fBasicEnergy;
				//Species* pOff = new Species(pSp->nMaxAge, pSp->fAliveEnergy, pSp->fBasicEnergy, pSp->nRank, pSp->fDayEnergy);
				//vPopulation.push_back(pOff);
				PutNew(pSp->nRank, nNum);
			}

			++pSp->nAge;
			if(pSp->nAge > pSp->nMaxAge)
			{
				delete pSp;
				continue;
			}

			vPopulation.push_back(pSp);
		}

		vOldPopulation.clear();


	}

	void Stats()
	{
		vector<int> vStats(nRanks, 0);
		for(size_t i = 0; i < vPopulation.size(); ++i)
			vStats[vPopulation[i]->nRank]++;

		ofstream ofs("out.txt", std::ios_base::in | std::ios_base::app);

		for(int j = 0; j < nRanks; ++j)
		{
			cout << vStats[j] << "\t";
			ofs << vStats[j] << "\t";
		}
		//cout << "\n";
		//ofs << "\n";
		ofs.close();
	}
};

struct Universes
{
	vector<Universe> v;

	void Advance()
	{
		size_t i;
		for(i = 0; i < v.size(); ++i)
			v[i].Live();
		for(i = 0; i < v.size(); ++i)
			v[i].Advance();
	}
	void Stats()
	{
		size_t i;
		for(i = 0; i < v.size(); ++i)
			v[i].Stats();
		ofstream ofs("out.txt", std::ios_base::in | std::ios_base::app);
		ofs << "\n";
	}
};

Universes unv;

void PutNew(int nRank, int nUnv)
{
	if(rand()%100 == 0)
		nUnv = (nUnv + 1)%unv.v.size();

	if(nRank == 0)
	{
		unv.v.at(nUnv).vPopulation.push_back(new Species(nMaxAge, .5, 1, 0, .1));
	}
	else
	{
		unv.v.at(nUnv).vPopulation.push_back(new Species(nMaxAge, 1, 2, 1, .7));
	}
}

int main()
{
    srand( (unsigned)time( NULL ));

	ofstream ofs("out.txt");
	ofs.close();

	unv.v.push_back(Universe(unv.v.size()));
	unv.v.push_back(Universe(unv.v.size()));

	PutNew(0, 0);
	PutNew(0, 1);

	bool bNonStop = false;
	while(true)
	{
		unv.Stats();
		
		if(!bNonStop)
			while(true)
			{
				char c = cin.get();
				if(c != 'a')
					break;
				PutNew(1, 0);
				PutNew(1, 1);
				//bNonStop = true;
			}
		unv.Advance();
	}

	return 0;
}