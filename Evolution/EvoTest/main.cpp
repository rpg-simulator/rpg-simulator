#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <time.h>

using namespace std;

const int nMaxAge = 30;
const float fUnitEnergy = 1.F;
const unsigned nUniverseSize = 10000;
const unsigned nRanks = 2;

int BigRand()
{
	return rand()*RAND_MAX + rand();
}

struct Species
{
	int nAge;
	int nMaxAge;
	float fAliveEnergy;
	float fBasicEnergy;
	float fDayEnergy;
	float fEnergy;

	int nRank;
	//int nCover;

	bool bDead;
	
	Species(int nMaxAge_, float fAliveEnergy_, float fBasicEnergy_, int nRank_, float fDayEnergy_)
		:nAge(0), nMaxAge(nMaxAge_), fAliveEnergy(fAliveEnergy_),
		fBasicEnergy(fBasicEnergy_), fEnergy(fBasicEnergy_), fDayEnergy(fDayEnergy_),
		nRank(nRank_), bDead(false)
	{}
};

struct Universe
{
	vector<Species*> vPopulation;
	vector< vector<Species*> > vWorld;

	Universe():vWorld(nUniverseSize, vector<Species*>()){}

	void Advance()
	{
		size_t i;
		for(i = 0; i < nUniverseSize; ++i)
			vWorld[i].clear();

		for(i = 0; i < vPopulation.size(); ++i)
			vWorld[BigRand()%nUniverseSize].push_back(vPopulation[i]);

		for(i = 0; i < nUniverseSize; ++i)
		{
			vector<Species*>& v = vWorld[i];

			vector< vector<Species*> > vSpeciesByRank(nRanks, vector<Species*>());
			
			for(size_t j = 0, sz = v.size(); j < sz; ++j)
				vSpeciesByRank[v[j]->nRank].push_back(v[j]);

			for(size_t nRank = 0; nRank < nRanks; ++nRank)
			{
				/*
				if(nRank == 0)
				{
					for(size_t k = 0, sz = vSpeciesByRank[nRank].size(); k < sz; ++k)
						vSpeciesByRank[nRank][k]->fEnergy += fUnitEnergy/sz;
					continue;
				}
				*/
				for(size_t k = 0, sz = vSpeciesByRank[nRank].size(); k < sz; ++k)
					vSpeciesByRank[nRank][k]->fEnergy += fUnitEnergy/v.size();

				if(nRank == 0)
				{
					continue;
				}

				//if(vSpeciesByRank[nRank].size() > 1 && rand()%12 > 7)
				//if(vSpeciesByRank[nRank].size() > 1 && rand()%2)
				//if(vSpeciesByRank[nRank].size() > 1)
				//	continue;

				for(size_t k = 0, sz = vSpeciesByRank[nRank].size(); k < sz; ++k)
				{
					if(vSpeciesByRank[nRank - 1].empty())
						continue;
					//if(vSpeciesByRank[nRank - 1].size() == 1 && rand()%2)
					//	continue;
					Species* pPrey = vSpeciesByRank[nRank - 1][rand()%vSpeciesByRank[nRank - 1].size()];
					if(pPrey->bDead)
						continue;
					pPrey->bDead = true;
					vSpeciesByRank[nRank][k]->fEnergy += pPrey->fEnergy;
				}
			}
		}

		vector<Species*> vOldPopulation = vPopulation;
		vPopulation.clear();

		for(i = 0; i < vOldPopulation.size(); ++i)
		{
			Species* pSp = vOldPopulation[i];
			
			if(pSp->bDead)
			{
				delete pSp;
				continue;
			}

			pSp->fEnergy -= pSp->fDayEnergy;
			if(pSp->fEnergy < pSp->fAliveEnergy)
			{
				delete pSp;
				continue;
			}

			while(pSp->fEnergy >= pSp->fBasicEnergy*2)
			{
				pSp->fEnergy -= pSp->fBasicEnergy;
				Species* pOff = new Species(pSp->nMaxAge, pSp->fAliveEnergy, pSp->fBasicEnergy, pSp->nRank, pSp->fDayEnergy);
				vPopulation.push_back(pOff);
			}

			++pSp->nAge;
			if(pSp->nAge > pSp->nMaxAge)
			{
				delete pSp;
				continue;
			}

			vPopulation.push_back(pSp);
		}


	}

	void Stats()
	{
		vector<int> vStats(nRanks, 0);
		for(size_t i = 0; i < vPopulation.size(); ++i)
			vStats[vPopulation[i]->nRank]++;

		ofstream ofs("out.txt", std::ios_base::in | std::ios_base::app);

		for(int j = 0; j < nRanks; ++j)
		{
			cout << vStats[j] << "\t";
			ofs << vStats[j] << "\t";
		}
		//cout << "\n";
		ofs << "\n";
		ofs.close();
	}
};

int main()
{
    srand( (unsigned)time( NULL ));

	ofstream ofs("out.txt");
	ofs.close();

	Universe u;

	u.vPopulation.push_back(new Species(nMaxAge, 5, 10, 0, 0));

	bool bNonStop = false;
	while(true)
	{
		u.Stats();
		
		if(!bNonStop)
			while(true)
			{
				char c = cin.get();
				if(c == 'a')
				{
					u.vPopulation.push_back(new Species(nMaxAge, 5, 10, 1, 7));
				}
				/*
				else if(c == 'b')
				{
					u.vPopulation.push_back(new Species(nMaxAge, 75, 150, 2, 1));
				}
				*/
				else
					break;
				//bNonStop = true;
			}
		u.Advance();
	}

	return 0;
}